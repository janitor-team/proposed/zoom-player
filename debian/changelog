zoom-player (1.1.5~dfsg-6) unstable; urgency=medium

  [ Reiner Herrmann ]
  * Update debian/watch.

  [ Stephen Kitt ]
  * Switch to debhelper compatibility level 13.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.5.1, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 05 Jan 2021 22:29:24 +0100

zoom-player (1.1.5~dfsg-5) unstable; urgency=medium

  * Migrate to Salsa.
  * Update debian/copyright.
  * Switch to debhelper compatibility level 11.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 14 May 2018 10:04:30 +0200

zoom-player (1.1.5~dfsg-4) unstable; urgency=medium

  * Add upstream patch to declare inline functions correctly, allowing the
    project to build with GCC 7. Closes: #853718.
  * Switch to debhelper compatibility level 10.
  * Standards-Version 4.1.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 05 Sep 2017 21:29:56 +0200

zoom-player (1.1.5~dfsg-3) unstable; urgency=medium

  * Only install relevant files for the manual (HTML files and images),
    not the full contents of the manual directory.
  * Clean up debian/control with cme.
  * Standards-Version 3.9.8, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 09 Jun 2016 12:10:15 +0200

zoom-player (1.1.5~dfsg-2) unstable; urgency=medium

  [ Alexandre Detiste ]
  * Revert to a version 3 watch file.
  * Avoid crashing on invalid coordinates when setting colours in V6 games
    (Zork Zero in particular). Closes: #813674.

  [ Stephen Kitt ]
  * Switch to https: VCS URIs (see #810378).
  * Enable anti-aliasing by default and recommend the appropriate X font
    packages. Closes: #813715.
  * Enable all hardening options.

 -- Stephen Kitt <skitt@debian.org>  Sat, 06 Feb 2016 17:22:52 +0100

zoom-player (1.1.5~dfsg-1) unstable; urgency=low

  * Initial release. Closes: #811026.

 -- Stephen Kitt <skitt@debian.org>  Sun, 17 Jan 2016 17:02:10 +0100
