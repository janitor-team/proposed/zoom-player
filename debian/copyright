Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: zoom
Source: http://www.logicalshift.co.uk/unix/zoom/
Files-Excluded:
 ProjectBuilder
 src/ifmetabase*
 src/ifmetaxml.*
 src/infocom.iFiction
 src/macos
 src/zoomCocoa
 ZoomCocoa.xcodeproj
 *.plist

Files: *
Copyright: 2000-2011 Andrew Hunter
License: GPL-2.0+

Files: debian/*
Copyright: 2016-2018, 2021 Stephen Kitt
License: GPl-2.0+

Files:
 INSTALL
 Makefile.in
 aclocal.m4
 bonus/Makefile.in
 builder/Makefile.in
 config.guess
 config.sub
 configure
 depcomp
 install-sh
 m4/Makefile.in
 manual/Makefile.in
 missing
 src/Makefile.in
Copyright: 1992-2013 Free Software Foundation, Inc.
           1994 X Consortium
License: permissive
 Auto-generated file under the permissive license.

Files:
 builder/main.c
 builder/operation.h
 src/blorb.c
 src/blorb.h
 src/debug.c
 src/debug.h
 src/display.c
 src/display.h
 src/eval.y
 src/file.c
 src/file.h
 src/font3.c
 src/font3.h
 src/hash.c
 src/hash.h
 src/iff.c
 src/ifmetadata.c
 src/ifmetadata.h
 src/image.h
 src/interp.c
 src/interp.h
 src/main.c
 src/menu.c
 src/menu.h
 src/options.c
 src/options.h
 src/random.c
 src/random.h
 src/rc.c
 src/rc.h
 src/rcp.h
 src/state.c
 src/state.h
 src/stream.c
 src/stream.h
 src/tokenise.c
 src/tokenise.h
 src/v6display.c
 src/v6display.h
 src/zmachine.c
 src/zmachine.h
 src/zscii.c
 src/zscii.h
Copyright: 2000 Andrew Hunter
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License Version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1'.

Files:
 builder/gram.c
 builder/gram.h
 src/eval.c
 src/eval.h
 src/rc_parse.c
 src/rc_parse.h
Copyright: 1984-2006 Free Software Foundation, Inc.
License: GPL-2.0+ with Bison exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof as
 a parser skeleton.  Alternatively, if you modify or redistribute the
 parser skeleton itself, you may (at your option) remove this special
 exception, which will cause the skeleton and the resulting Bison
 output files to be licensed under the GNU General Public License
 without this special exception.
 .
 This special exception was added by the Free Software Foundation in
 version 2.2 of Bison.
 .
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in `/usr/share/common-licenses/GPL-2'.

Files:
 src/md5.c
 src/md5.h
Copyright: 1999-2002 Aladdin Enterprises.
License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must
 not be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source
 distribution.

Files: ylwrap
Copyright: 1996-2009 Free Software Foundation, Inc.
License: GPL-2.0+ with Autoconf exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 As a special exception to the GNU General Public License, if you
 distribute this file as part of a program that contains a
 configuration script generated by Autoconf, you may include it under
 the same distribution terms that you use for the rest of that
 program.
 .
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in `/usr/share/common-licenses/GPL-2'.


License: GPL-2.0+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in `/usr/share/common-licenses/GPL-2'.
